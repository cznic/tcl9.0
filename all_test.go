// Copyright 2024 The tcl9.0-go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package tcl9_0 // import "modernc.org/tcl9.0"

import (
	"flag"
	"fmt"
	"os"
	"strings"
	"testing"

	lib "modernc.org/libtcl9.0"
)

func TestMain(m *testing.M) {
	flag.Parse()
	os.Exit(m.Run())
}

func TestEval(t *testing.T) {
	in, err := NewInterp(map[string]string{"tcl_library": MustStdlib()})
	if err != nil {
		t.Fatal(err)
	}

	defer func() {
		if err := in.Close(); err != nil {
			t.Error(err)
		}
	}()

	s, err := in.Eval("set a 42; incr a", EvalGlobal)
	if err != nil {
		t.Fatal(err)
	}

	if g, e := s, "43"; g != e {
		t.Errorf("got %q exp %q", g, e)
	}
}

// Configure a new Tcl interpreter to use the default Tcl stdlib. Error
// handling omitted.
func Example() {
	in, _ := NewInterp(map[string]string{"tcl_library": MustStdlib()})
	fmt.Println(in.Eval("package require http;", EvalGlobal))
	// Output: 2.10.0 <nil>
}

func ExampleInterp_RegisterCommand() {
	in, _ := NewInterp(map[string]string{"tcl_library": MustStdlib()})
	var delTrace string
	in.RegisterCommand(
		"::go::echo",
		func(clientData interface{}, in *Interp, args []string) int {
			// Go implementation of the Tcl ::go::echo command
			args = append(args[1:], fmt.Sprint(clientData))
			in.SetResult(strings.Join(args, " "))
			return lib.TCL_OK
		},
		42, // client data
		func(clientData interface{}) {
			// Go implemetation of the command delete handler
			delTrace = fmt.Sprint(clientData)
		},
	)
	fmt.Println(in.Eval("::go::echo 123 foo bar", EvalGlobal))
	in.Close()
	fmt.Println(delTrace)
	// Output:
	// 123 foo bar 42 <nil>
	// 42
}
