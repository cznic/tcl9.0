# tcl9.0

Package tcl9.0 is an idiomatic Go wrapper for [libtcl9.0].

[libtcl9.0]: https://pkg.go.dev/modernc.org/libtcl9.0
